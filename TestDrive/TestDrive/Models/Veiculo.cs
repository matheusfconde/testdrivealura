﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestDrive.Models
{
    public class Veiculo
    {
        public const int freioAbs = 800;
        public const int arCondicionado = 1000;
        public const int mp3Player = 500;

        public string Nome { get; set; }
        public decimal Preco { get; set; }

        public string PrecoFormatado
        {
            get { return string.Format("R$ {0}", Preco); }
        }

        public bool TemFreioAbs { get; set; }
        public bool TemArCondicionado { get; set; }
        public bool TemMp3Player { get; set; }

        public String PrecoTotalFormatado
        {
            get
            {
                return string.Format("Valor total: R$ {0}",
                    Preco
                    + (TemFreioAbs ? freioAbs : 0)
                    + (TemMp3Player ? mp3Player : 0)
                    + (TemArCondicionado ? arCondicionado : 0));
            }
        }
    }
}
